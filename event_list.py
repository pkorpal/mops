import numpy
from math import inf
from arrival import Arrival
from service import Service
from const import lam, mi


class EventList:

    def __init__(self):
        self.events = []
        self.time = -1

    def add_event(self, event):
        if event == 'arrival':
            self.events.append(Arrival(self.time + self.gen_arrival_time()))
        elif event == 'service':
            self.events.append(Service(self.time + self.gen_arrival_time(), self.gen_service_time()))

    def pop_event(self):
        if len(self.events) == 0:
            return None

        tmp_event = None
        tmp_time = inf

        for event in self.events:
            if event.time < tmp_time:
                tmp_event = event
                tmp_time = event.time

        self.events.remove(tmp_event)
        self.time = tmp_time

        return tmp_event

    def next_event_time(self):
        tmp = inf
        
        for event in self.events:
            if event.time < tmp:
                tmp = event.time

        return tmp

    def gen_arrival_time(self):
        return numpy.random.poisson(lam)
    
    def gen_service_time(self):
        return numpy.random.poisson(mi)