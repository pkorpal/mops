import matplotlib.pyplot as plt
from arrival import Arrival
from router import Router
from const import queue_size, lam, mi
import sys
from termcolor import colored

r3 = Router('r3', False, None)
r2 = Router('r2', False, r3)
r1 = Router('r1', True, r2)

def findNextRouter():
    t1 = r1.event_list.next_event_time()
    t2 = r2.event_list.next_event_time()
    t3 = r3.event_list.next_event_time()

    time = min(t1,t2,t3)

    if time == t1:
        return r1
    elif time == t2:
        return r2
    elif time == t3:
        return r3

r1.event_list.add_event('arrival')
i = 0
while i < 1000000:
    router = findNextRouter()
    router.proccess_next_event()
    i += 1

print(colored("Dane:", 'red'))
print(colored(f"    λ", 'blue') + f" = {lam}, " + colored("µ ", 'blue')  + f"= {mi}, " + colored("queue_size", 'blue') + f" = {queue_size}")
print("")
print(colored("Wyniki teoretyczne:", 'red'))
print(colored(f"    servicePrec", 'blue') + f" = {lam/mi*100:.3f} [%]")
print("")
print(colored("Wyniki symulacji:", 'red'))
print("r1:")
r1.status()
print("r2:")
r2.status()
print("r3:")
r3.status()