from event_list import EventList
from arrival import Arrival
from service import Service
from const import queue_size
from termcolor import colored
import matplotlib.pyplot as plt

class Router:
    def __init__(self, name, connected_to_host, next_hop):
        self.pkgs_in_queue = 0
        self.name = name
        try:
            self.next_hop_name = next_hop.name
        except:
            self.next_hop_name = None
        self.event_list = EventList()
        self.queue_size = queue_size
        
        self.pkgs_arrived = 0
        self.pkgs_serviced = 0
        self.pkgs_dropped = 0
        self.service_time = 0
        
        self.connected_to_host = connected_to_host
        self.next_hop = next_hop
        self.print_router_data()
        self.drop_packages_array = []
        self.service_time_array = []

    def print_router_data(self):
        print(colored("Printing Router data", 'red'))
        print(colored("Router", 'green') + f": {self.name}")
        print(colored("Connected to host", 'green') + f": {self.connected_to_host}")
        print(colored("Next hop", 'green') + f": {self.next_hop_name}")
        print(colored("Queue size", 'green') + f": {self.queue_size}")

    def proccess_next_event(self):
        tmp_event = self.event_list.pop_event()

        if tmp_event is None:
            return
        elif tmp_event.name == 'arrival':
            self.arrival(tmp_event)
        elif tmp_event.name == 'service':
            self.service(tmp_event)

    def arrival(self, event):
        self.pkgs_arrived += 1
        if self.pkgs_in_queue == self.queue_size:
            self.pkgs_dropped += 1
            self.drop_packages_array.append(self.pkgs_dropped)
            self.service_time_array.append(event.time / 1000)
            if self.connected_to_host == True:
                self.event_list.add_event('arrival')
            return

        self.pkgs_in_queue += 1

        if self.pkgs_in_queue == 1:
            self.event_list.add_event('service')

        if self.connected_to_host:
            self.event_list.add_event('arrival')
        if self.next_hop is not None:
            self.next_hop.event_list.add_event(event)

    def service(self, event):
        self.pkgs_in_queue -= 1
        self.pkgs_serviced += 1
        self.service_time += event.service_time

        if self.pkgs_in_queue > 0:
            self.event_list.add_event('service')
        if self.next_hop is not None:
            self.next_hop.event_list.add_event('arrival')

    def show_dropped_packages_plot(self, x, y):
        plt.plot(x, y, 'r')
        plt.title(f"Router {self.name} dropped packages")
        plt.xlabel("time [u]")
        plt.ylabel("dropped packages")
        plt.show()

    def status(self):
        pkgs_per_sec = self.pkgs_serviced / self.event_list.time * 1000
        service_perc = self.service_time / self.event_list.time * 100

        try:
            pkgs_dropped_perc = self.pkgs_dropped / self.pkgs_arrived
        except:
            pkgs_dropped_perc = 0
        print(colored("pkgs_arrived ", 'blue') + f"= {self.pkgs_arrived}")
        print(colored("pkgs_serviced ", 'blue') + f"= {self.pkgs_serviced}")
        print(colored("pkgs_dropped ", 'blue') + f"= {self.pkgs_dropped}")
        print(colored("pkgs_dropped_perc ", 'blue') + f"= {pkgs_dropped_perc:.3f} [%]")
        print(colored("server_usage ", 'blue') + f"= {service_perc:.3f} [%]")
        self.show_dropped_packages_plot(self.service_time_array, self.drop_packages_array)